// console.log("Hello World!");

function printWelcomePopup(){
let namePrompt = prompt("What is your name?");
let agePrompt = prompt("How old are you?");
let locationPrompt = prompt("Where do you live?");

console.log("Hello, " + namePrompt + "!");
console.log("You are " + agePrompt + " years old.");
console.log("You live at " + locationPrompt + ".");

};

printWelcomePopup();

function thankYouPopup(){
	alert("Thank you for your input!");
};

thankYouPopup();

function myForeverArtists(){
	let favArtists1 = "Taylor Swift"
	let favArtists2 = "Bruno Mars"
	let favArtists3 = "Avril Lavigne"
	let favArtists4 = "BTS"
	let favArtists5 = "Blackpink"

	console.log("My top 5 favorite artists: ");
	console.log("1. " + favArtists1);
	console.log("2. " + favArtists2);
	console.log("3. " + favArtists3);
	console.log("4. " + favArtists4);
	console.log("5. " + favArtists5);
};

myForeverArtists();

function myNeverEndingMovie(){

	let bestMovie1 = "Avengers: Endgame"
	let bestMovie2 = "Avengers: Infinity War"
	let bestMovie3 = "Doctor Strange in the Multiverse of Madness"
	let bestMovie4 = "Free Guy"
	let bestMovie5 = "Miss Peregrine's Home for Peculiar Children"

	console.log("My top 5 favorite movies: ");
	console.log("1. " + bestMovie1 + "\n Rotten Tomatoes Rating: 94%");
	console.log("2. " + bestMovie2 + "\n Rotten Tomatoes Rating: 85%");
	console.log("3. " + bestMovie3 + "\n Rotten Tomatoes Rating: 74%");
	console.log("4. " + bestMovie4 + "\n Rotten Tomatoes Rating: 80%");
	console.log("5. " + bestMovie5 + "\n Rotten Tomatoes Rating: 64%");
};

myNeverEndingMovie();


function addFriends(){
	alert("Hi! Please add the names of your friends.");
};

addFriends();

function printFriends(){

	let bff1 = prompt("Enter your first friend's name: "); 
	let bff2 = prompt("Enter your second friend's name: "); 
	let bff3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with: ");
	console.log(bff1); 
	console.log(bff2); 
	console.log(bff3); 
};

printFriends();